package Main;

import Managers.ConfigManager;
import Utils.Utils;

import java.io.Console;
import java.util.Scanner;

public class RFID_Writer {

    private static Scanner scanner;
    public static int counter;

    /**
     * The Main
     * @param args
     */
    public static void main(String[] args) {

        try {
            //SoundManager.demo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ----

        Utils.log("App started");
        Utils.log("OS: " + System.getProperty("os.name") + ", " + System.getProperty("os.arch"));
        ConfigManager.getInstance().getAppConfig();

        doProcess();

    }

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    /**
     *  Do process
     */
    private static void doProcess(){

        Worker.init();

        scanner = new Scanner(System.in);

        Utils.message("Add meg az első RF ID-t (Pl. 100)!\n: ");
        counter = getCounter();
        if (counter == 0 ) {
            Utils.message("Kiléptél az alkalmazásból\n");
            return;
        }

        while (true){
            Utils.message("================================================================================================");
            Utils.message("A következő azonosító: " + counter);
            Utils.message("Tedd a TAG-et az antenna elé, majd nyomj 'ENTER' +t! Kilépés: 'x'");
            scanner = new Scanner(System.in);
            String inp = scanner.nextLine();

            if (inp.toLowerCase().contains("x")) {
                Utils.message("Kiléptél az alkalmazásból\n");
                break;
            } else {
                Worker.sendAntennaRequest();
                Utils.log(" +++++++++++++++++ ");
            }
        }
    }

// *******************************************************************************************************************************************
// * UTILS
// *******************************************************************************************************************************************
    /**
     * get first ID
     * @return
     */
    private static int getCounter(){
        int ret = 0;

        try {
            String inp = scanner.next();
            if (inp.toLowerCase().contains("x")) {
                return 0;
            }
            ret = Integer.parseInt(inp);
        } catch (Exception e) {
            //e.printStackTrace();
            Utils.log("Hibás adat, kilépés ....");
        }

        return ret;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    public static void waitForEnter() {
        Console c = System.console();
        if (c != null) {
            // printf-like arguments
//            if (message != null)
//                c.format(message, args);
//            c.format("\nPress ENTER to proceed.\n");
            c.readLine();
        }
    }

}
