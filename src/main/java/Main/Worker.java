package Main;

import Custom.ConsoleColors;
import DTOs.rfid.RfData;
import Helpers.WorkerHelper;
import Interfaces.ISerialEvent;
import Managers.ConfigManager;
import Managers.SerialManager;
import Managers.SoundManager;
import Utils.Utils;
import com.fazecast.jSerialComm.SerialPortEvent;

import javax.sound.sampled.LineUnavailableException;
import java.util.ArrayList;

import static Managers.SerialManager.serialPort;

/**
 * Az egyes parancs kódok, parancsok, válaszok kezeléséhez olvas el az alábbi PDF file-t
 * Serial_Protocol_User's_Guide_V2.38_en .pdf
 */
public class Worker {

    public static final int BUFFER_MODE_WRITE = 0;
    public static final int BUFFER_MODE_READ = 1;
    public static final int BUFFER_MODE_DELETE = 2;

    public static final byte COMMAND_SET_ANTENNE = (byte) 0x74;
    public static final byte COMMAND_READ = (byte) 0x81;
    public static final byte COMMAND_WRITE = (byte) 0x82;

    private static ArrayList<Byte> tmpBuffer = new ArrayList<>();
    public static ArrayList<RfData> rfDataArr = new ArrayList<>();

    private static int sizeFull;
    private static int sizeOfficial;
    private static int cmd;
    private static boolean firstRead;

    public static void init() {
        // Starting to list serial port
        listenSerialPort();
        byte[] params = {0};                 // Antenna 0.
    }

// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    /**
     * handleBuffer: WRITE, READ, DELETE
     * @param mode
     * @param serialIncomingData
     */
    public synchronized static boolean handleBuffer(int mode, byte[] serialIncomingData) {

        //Utils.log(" ***** ");

        WorkerHelper.mergeToTemp(tmpBuffer, serialIncomingData);                    // Merging incoming data to temp
        byte[] encodedTmpBuffer = WorkerHelper.createEncodedTmpBuffer(tmpBuffer);   // full tmp buffer in byte Arr

        getCmd();

        if (cmd == COMMAND_SET_ANTENNE) {                                             // If cmd is Antenna
            // 2. 1épés. Antenna váltás, antenna számláló inkrementálása
            if (tmpBuffer.size() >= sizeFull) {                                     // buffer size > fullSize
                Utils.log(ConsoleColors.BLUE_BOLD + "IN <- Antenna data received, sizefull: " + sizeFull + ", size official: " + sizeOfficial + ConsoleColors.RESET);

                WorkerHelper.deleteFromTmp(tmpBuffer, sizeFull);
                firstRead = true;
                // Sending read command
                byte[] param = {0x01, 0x02, 0x01};
                SerialManager.sendToCommPort(COMMAND_READ, param, serialPort);

            } else {
                Utils.log("//////////////////");
            }
        } else if (cmd == COMMAND_READ) {                                  // If cmd is Read inventory
            //2 - 4. lépés, tagek beolvasása

            if (tmpBuffer.size() >= sizeFull && tmpBuffer.size() == 29) {                                     // buffer size > fullSize
                int id = WorkerHelper.getID(tmpBuffer, sizeFull);
                Utils.log(ConsoleColors.BLUE_BOLD + "IN <- RF Tag data, sizefull: " + sizeFull + ", size official: " + sizeOfficial + ConsoleColors.RESET);
                Utils.log(ConsoleColors.BLUE_BOLD + "ID: " + id + ConsoleColors.RESET);
                WorkerHelper.deleteFromTmp(tmpBuffer, sizeFull);

                byte[] idArr = Utils.intToBytes(RFID_Writer.counter);

                if (firstRead) {
                    firstRead = false;
                    byte[] param = {
                            0x00, 0x00, 0x00, 0x00,         //Password
                            0x01,                           //EPC
                            0x02,
                            0x01,
                            idArr[2],
                            idArr[3]
                    };
                    SerialManager.sendToCommPort(COMMAND_WRITE, param, serialPort);
                } else {
                    if (id == RFID_Writer.counter) {
                        Utils.log(ConsoleColors.GREEN_BOLD + "SIKERES ÍRÁS!, ID: " + id + ConsoleColors.RESET);
                        try {
                            SoundManager.tone(1000,100);
                            SoundManager.tone(2000,100);
                            RFID_Writer.counter++;
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        }
                        Thread.currentThread().interrupt();
                        return true;
                    } else {
                        Utils.log(ConsoleColors.RED_BOLD + "NEM SIKERÜLT AZ ÍRÁS, id = " + id + ConsoleColors.RESET);
                        try {
                            SoundManager.tone(400,400);
                            SoundManager.tone(300,200);
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                }
            } else {
                Utils.log(ConsoleColors.RED_BOLD + "NEM ÉRZÉKELHETŐ RF TAG !" + ConsoleColors.RESET);
                tmpBuffer.clear();
                return false;
            }
        } else if( cmd == COMMAND_WRITE){
            // Sending read command
            tmpBuffer.clear();
            byte[] param = {0x01, 0x02, 0x01};
            SerialManager.sendToCommPort(COMMAND_READ, param, serialPort);
        } else {                                                                    // If the command is unknown
            //Utils.log(ConsoleColors.RED + "!!! BUFFER ERROR 2. Buffer has an error, it must be deleted." + cmd + ConsoleColors.RESET);
            tmpBuffer.clear();
        }

        return false;
    }


// *******************************************************************************************************************************************
// * FUNCTIONS
// *******************************************************************************************************************************************
    private static boolean getCmd(){

        boolean ret = false;

        sizeFull = 2;
        cmd = 0;

        if(tmpBuffer.size() == 0){                                                  // BUFFER CHECK - 0 size, error, return
            //Utils.log("BUFFER MODE WRITE BREAK 1");
            return false;
        }

        if(tmpBuffer.get(0) != (byte) 0xA0){                                        // HEAD CHACK - No head, error, return
            //Utils.log(ConsoleColors.RED + "!!! BUFFER ERROR 1. Buffer has an error, it must be deleted." + HexBin.encode(encodedTmpBuffer) + ConsoleColors.RESET);
            WorkerHelper.smartClear(tmpBuffer);
            return false;
        }

        if (tmpBuffer.size() > 1 ){                                                 // OFFICIAL PACKAGE SIZE
            sizeOfficial = tmpBuffer.get(1).intValue() & 0xff;                      // Official package size
            sizeFull += sizeOfficial;                                               // Full, real package size = 2 + official size
        } else {
            //Utils.log("BUFFER MODE WRITE BREAK 2");
            return false;
        }

        if (tmpBuffer.size() > 3 ){
            cmd = tmpBuffer.get(3);
            ret = true;
        } else {
            //Utils.log("BUFFER MODE WRITE BREAK 3");
            return false;
        }

        return ret;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Send antenna request
     */
    public static void sendAntennaRequest(){
        byte[] params = {(byte)0};
        SerialManager.sendToCommPort(COMMAND_SET_ANTENNE, params, serialPort);
        Utils.log(ConsoleColors.GREEN + "ANTENNA: 0" + ConsoleColors.RESET);
    }

// *******************************************************************************************************************************************
// * EVENTS
// *******************************************************************************************************************************************
    /**
     * Open and listen serial port
     */
    private static void listenSerialPort(){
        SerialManager.openSerialPort(new ISerialEvent() {
            @Override
            public void onResponse(SerialPortEvent serialPortEvent) {

                int bytesAvailable = serialPort.bytesAvailable();
                byte[] serialIncomingData = new byte[bytesAvailable];
                serialPort.readBytes(serialIncomingData, bytesAvailable);

                //Utils.log("listenSerialPort / openSerialPort / onResponse, bytes available: " + bytesAvailable + ", buffersize now: " + serialPort.bytesAvailable());

                handleBuffer(BUFFER_MODE_WRITE, serialIncomingData);

            }
        });
    }

}
