package Managers;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import java.util.ArrayList;

public class SoundManager {

    public static float SAMPLE_RATE = 8000f;

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * tone
      * @param hz
     * @param msecs
     * @throws LineUnavailableException
     */
    public static void tone(int hz, int msecs) throws LineUnavailableException {
        tone(hz, msecs, 1.0);
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Tone 2
     * @param hz
     * @param msecs
     * @param vol
     * @throws LineUnavailableException
     */
    public static void tone(int hz, int msecs, double vol) throws LineUnavailableException {

        if(!ConfigManager.getInstance().getAppConfig().sound.sound_enabled){
            return;
        }

        byte[] buf = new byte[1];
        AudioFormat af = new AudioFormat(
            SAMPLE_RATE, // sampleRate
            8,           // sampleSizeInBits
            1,           // channels
            true,        // signed
            false);      // bigEndian
        SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
        sdl.open(af);
        sdl.start();
        for (int i=0; i < msecs*8; i++) {
            double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
            buf[0] = (byte)(Math.sin(angle) * 127.0 * vol);
            sdl.write(buf,0,1);
        }
        sdl.drain();
        sdl.stop();
        sdl.close();
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     *
     * @throws Exception
     */
    public static void demo() throws Exception{

        if(!ConfigManager.getInstance().getAppConfig().sound.sound_enabled){
            return;
        }

        SoundManager.tone(1000,200);
//        Thread.sleep(1000);
//        SoundManager.tone(100,1000);
//        Thread.sleep(1000);
//        SoundManager.tone(5000,100);
//        Thread.sleep(1000);
//        SoundManager.tone(400,500);
//        Thread.sleep(1000);
//        SoundManager.tone(400,500, 0.2);
    }
}
