package Managers;

import DTOs.Config;
import Utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * App congfig manager is for giving a right configuration based on Config DTO / config file
 */
public class ConfigManager {

    private static Config config = new Config();
    private static ConfigManager instance = null;

    public static ConfigManager getInstance(){
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    /**
     * GetAppCongi gives default / loaded configuration for ports.
     * The default is not really usable. Yo may override it.
     * @return
     */
    public Config getAppConfig(){
        if (!config.isConfigAlreadyLoaded) {
            Properties prop = new Properties();
            InputStream input = null;

            try {
                File jarPath=new File(ConfigManager.class.getProtectionDomain().getCodeSource().getLocation().getPath());
                String propertiesPath=jarPath.getParentFile().getAbsolutePath();
                prop.load(new FileInputStream(propertiesPath+"/default.cfg"));

                config.comm.serial_windows_keyword = prop.getProperty("serial_windows_keyword");
                config.comm.serial_linux_keyword = prop.getProperty("serial_linux_keyword");
                config.comm.serial_port_baud_rate = Integer.parseInt(prop.getProperty("serial_port_baud_rate"));

                for (int i = 0; i < 4; i++) {
                    Config.Antenna antenna = new Config.Antenna();
                    // All antennas
                    antenna.an_installed = Boolean.parseBoolean(prop.getProperty(String.format("an0%s_installed", Integer.toString(i))));
                    config.antennas.add(antenna);

                    //Installed antenna
                    if (antenna.an_installed) {
                        config.installedAnteannaInputs.add(i);
                    }
                }

                config.sound.sound_enabled = Boolean.parseBoolean(prop.getProperty("sound_enabled"));

                config.reader_repeats = Integer.parseInt(prop.getProperty("reader_repeats"));

                Utils.log("Application config has been loaded from "+ propertiesPath + ". \n" + config.toString());
                config.isConfigAlreadyLoaded = true;

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return config;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Has installed antenna
     * @return
     */
    public static boolean hasInstalledAntennas(){
        return config.installedAnteannaInputs != null && config.installedAnteannaInputs.size() > 0;
    }
}
