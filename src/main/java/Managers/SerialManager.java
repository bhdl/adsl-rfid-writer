package Managers;

import Custom.ConsoleColors;
import DTOs.Config;
import Interfaces.ISerialEvent;
import Main.Worker;
import Utils.Utils;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * SerialManager
 */
public class SerialManager {

    public static SerialPort serialPort;
    private static ISerialEvent iSerialEvent;

    /**
     * openSerialPort
     * Opens seria port by os and app config parameters
     */
    public static void openSerialPort(ISerialEvent _iSerialEvent){
        iSerialEvent = _iSerialEvent;

        SerialPort ret = null;
        Config config = ConfigManager.getInstance().getAppConfig();
        String portname = null;

        String os = HardwareManager.getOs();
        if (os.equals(HardwareManager.OS_WINDOWS)) {
            serialPort = HardwareManager.getSerialPortByKeyword(config.comm.serial_windows_keyword);
            Utils.log("Port name 1 : " + portname);
        } else if (os.equals(HardwareManager.OS_LINUX)){
            serialPort = HardwareManager.getSerialPortByKeyword(config.comm.serial_linux_keyword);
            Utils.log("Port name 2 : " + portname);
        } else {
            Utils.log("Error. Name of operation system can not be detected");
        }

        if (serialPort != null) {
            portname = serialPort.getSystemPortName();
            serialPort = SerialPort.getCommPort(portname);
            serialPort.setComPortParameters(config.comm.serial_port_baud_rate, 8, 1, 0);
            serialPort.addDataListener(serialPortDataListener);
            serialPort.openPort();
        }

        Utils.log("Serial1 manager Just a stop");

    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Send commands to serialport
     * @param command
     * @param parameters
     */
     public static void sendToCommPort(byte command, byte[] parameters, SerialPort serialPort){

        if (serialPort == null){
            Utils.logError("SerialManager / sendToCommPort: Serial port is null" );
            return;
        }

        byte head = (byte) 0xA0;
        byte len = (byte) (1 + 1 + parameters.length + 1);  //Address + Command + parameters + checksum
        byte address = (byte) 0x01;
        byte checksumPlaceHolder = (byte) 0x00;

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
            outputStream.write( head );
            outputStream.write( len );
            outputStream.write( address );
            outputStream.write( command );
            outputStream.write( parameters );
            outputStream.write( checksumPlaceHolder );
            byte packet[] = outputStream.toByteArray( );

            byte checksum = Utils.getChecksum(packet);
            packet[packet.length -1 ] = checksum;

            //writeout
            serialPort.writeBytes(packet, packet.length);

            if (command == Worker.COMMAND_SET_ANTENNE) {
                Utils.log(ConsoleColors.BLUE + "-> OUT AN->: " + HexBin.encode(packet) + ConsoleColors.RESET);
            } else if (command == Worker.COMMAND_READ){
                Utils.log(ConsoleColors.BLUE + "-> OUT READ->: " + HexBin.encode(packet) + ConsoleColors.RESET);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Close opened serial port
     */
    public static void closeSerialPort(){
        if (serialPort != null && serialPort.isOpen()) {
            serialPort.closePort();
        }
    }

// *******************************************************************************************************************************************
// * Events
// *******************************************************************************************************************************************
    /**
     * Serial port data listener
     */
    private static SerialPortDataListener serialPortDataListener = new SerialPortDataListener() {
        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
        }

        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            if (serialPortEvent.getEventType() == SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
                // Incoming data
                iSerialEvent.onResponse(serialPortEvent);
            }

        }
    };
}
