package Managers;

import Utils.Utils;
import com.fazecast.jSerialComm.SerialPort;


/**
 * Hardware manager by scsaba
 *
 * The serial communication is baed on:
 * http://fazecast.github.io/jSerialComm/
 *
 */
public class HardwareManager {

    public static String OS_WINDOWS = "windows";
    public static String OS_LINUX = "linux";

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Get Os name:
     * "windows", "linux"
     * @return
     */
    public static String getOs(){
        String ret = "unknown";
        String os = System.getProperty("os.name");
        if (os.toLowerCase().contains(OS_WINDOWS)) {
            ret = OS_WINDOWS;
        } else if (os.toLowerCase().contains(OS_LINUX)){
            ret = OS_LINUX;
        }
        return ret;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * listing serial ports
     * @return
     */
    public static void listSerialPorts(){
        SerialPort[] serialPorts = SerialPort.getCommPorts();
        Utils.log("Available serial ports /=======================");
        for (SerialPort serialPort : serialPorts) {
            Utils.log(serialPort.getDescriptivePortName() + ", " + serialPort.getSystemPortName());
        }
        Utils.log("=======================");
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Get Serialport by keyword
     * It is used to open the proper serial port on Windows or Linux
     * @param keyword
     * @return
     */
    public static SerialPort getSerialPortByKeyword(String keyword){
        SerialPort ret = null;
        SerialPort[] serialPorts = SerialPort.getCommPorts();
        for (SerialPort serialPort : serialPorts) {
            if (serialPort.getSystemPortName().toLowerCase().contains(keyword.toLowerCase())) {
                ret = serialPort;
                break;
            }
        }
        return ret;
    }
}
