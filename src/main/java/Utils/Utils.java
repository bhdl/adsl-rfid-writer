package Utils;

import Custom.ConsoleColors;
import Managers.ConfigManager;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class Utils {

    /**
     * standard log
     * @param message
     */
    public static void log(String message){
        String currenTimeInMilis = Utils.getFormattedDateByMsec(System.currentTimeMillis());
        System.out.println(currenTimeInMilis + " " + message);
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * message
     * @param message
     */
    public static void message(String message){
        System.out.println(message);
    }


// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Log Error
     * @param message
     */
    public static void logError(String message){
        System.out.println(ConsoleColors.RED + "SCSI ....... " + message + ConsoleColors.RESET );
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * getChecksum
     * Calculates checksum
     * @param data
     * @return
     */
    public static byte getChecksum(byte[] data){
        byte checksum = 0;
        try {
            // Checksum -last byte - excluded
            for (int i = 0; i < data.length -1; i++) {
                checksum = (byte) (checksum + data[i]);
            }
            checksum = (byte) ((~checksum)+1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return checksum;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * int to ByteArray
     * @param i
     * @return
     */
    public static byte[] intToBytes( final int i ) {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(i);
        return bb.array();
    }

// *******************************************************************************************************************************************
// * DATE TIME
// *******************************************************************************************************************************************
    /**
     * getFormattedDateByMsec
     * Format is: yyyy_MM_dd_hh_mm_ss_ms
     * @param mSec
     * @return
     */
    public static String getFormattedDateByMsec(long mSec){

        try {
            SimpleDateFormat sdfOut;
            Date date = new Date(mSec);
            sdfOut = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_SSS");
            return sdfOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

// *******************************************************************************************************************************************
// * Threads
// *******************************************************************************************************************************************
    /**
     * List threads
     */
    public static void listThreads(){

        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);

        log("=======================================================");
        for (int i = 0; i< threadArray.length; i++) {
            log(threadArray[i].toString());
        }
        log("=======================================================");
    }

}
