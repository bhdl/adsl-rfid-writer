package DTOs;

import java.util.ArrayList;

/**
 * Data Class for Application Configuration with value defaults
 * App config must be read a the beginning
 */
public class Config {

    public boolean isConfigAlreadyLoaded = false;

    public Comm comm = new Comm();
    public ArrayList<Antenna> antennas = new ArrayList<>();
    public ArrayList<Integer> installedAnteannaInputs = new ArrayList<>(); //(0 - 3)
    public Sound sound = new Sound();

    // Others
    public int max_tags=100;
    public long flush_timeout=5000L;
    public int reader_repeats=2;

    public String toString(){

        try {
            return ("\nCONFIG SETTINGS ---" +
                    "\n  COMMUNICATION SETTINGS" +
                    "\n    serial_windows_keyword: " + comm.serial_windows_keyword +
                    "\n    serial_linux_keyword: " + comm.serial_linux_keyword +
                    "\n    serial_port_baud_rate: " + comm.serial_port_baud_rate +
                    "\n  ANTENNA INSTALLATIONS" +
                    "\n    an00_installed: " + antennas.get(0).an_installed +
                    "\n    an01_installed: " + antennas.get(1).an_installed +
                    "\n    an02_installed: " + antennas.get(2).an_installed +
                    "\n    an03_installed: " + antennas.get(3).an_installed +
                    "\n  SOUND MONITOR" +
                    "\n    sound_enabled: " + antennas.get(3).an_installed +
                    "\n  OTHERS" +
                    "\n    reader_repeats: " + reader_repeats +
                    "\n/---" +
                    "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

// *******************************************************************************************************************************************
// * INNER SUBCLASSES
// *******************************************************************************************************************************************
    /**
     * For the communication between onboard serial, and remote TCP/IP
     */
    public class Comm{

        public String serial_windows_keyword = "";      // Windows
        public String serial_linux_keyword = "";        // Linux
        public int serial_port_baud_rate = 0;           // default should be 115200 for reading RF module

    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Antenna installation and power level
     */
    public static class Antenna {
        public boolean an_installed=false;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * For client / sound monitor
     */
    public class Sound{
        public boolean sound_enabled = false;
    }

}
 