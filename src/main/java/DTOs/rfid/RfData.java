package DTOs.rfid;

/**
 * RfData
 * Parent -> Data
 */
public class RfData {
    public String tagId = "";               // 4 karakter. Formátum pl: "rf02"
    public String antennaId = "";           // 4 karakter. Formátum pl: "an02"
    public int rssi = 0;
    public long timestamp = 0L;             // in msec
}
