package DTOs.rfid;

/**
 * RfPackage
 */
public class RfPackage {
    public long timestamp = 0L;
    public String clientId = "";        // 4 karakter. Formátum pl: "cl03"
    public final String type = "rfid";  // fix, final!!
    public String packageId = "";       // A mentett fájl neve kiterjesztés nélkül
    public Data data = new Data();
}
