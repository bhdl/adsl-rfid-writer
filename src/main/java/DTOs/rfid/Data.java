package DTOs.rfid;

import java.util.ArrayList;

/**
 * Data
 * Parent -> RfPackage
 */
public class Data {
    public ReaderStatus readerStatus = new ReaderStatus();
    public ArrayList<RfData>rfData = new ArrayList<>();
}
