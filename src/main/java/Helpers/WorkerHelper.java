package Helpers;

import DTOs.rfid.RfData;
import Utils.Utils;
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

import java.util.ArrayList;

public class WorkerHelper {


    /**
     * Retrieves next valid tag from the buffer
     * @return
     */
    public static int getID(ArrayList<Byte> tmpBuffer, int sizeFull){

        int id = 0;
        byte check = tmpBuffer.get(sizeFull -1);

        byte[] fullChunk = new byte[sizeFull];
        for (int i = 0; i < sizeFull; i++) {
            fullChunk[i] = tmpBuffer.get(i);
        }

        byte computedChecksum = Utils.getChecksum(fullChunk);
        if (computedChecksum == check) {
            try {
                int offset = 9;                           // EPC id starts from 7. bytes
                int sizeData = sizeFull -9 -8;            // 9 + 8
                byte[] epcArr = new byte[sizeData];       //Id for encoding to HEX
                for (int i = 0; i < sizeData  ; i++) {
                    epcArr[i] = tmpBuffer.get(i + offset);
                }

                if (sizeData == 12) {                     // Valid data size must be 12
                    String epc = HexBin.encode(epcArr);
                    id = Integer.parseInt(epc.substring(0,4), 16);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return id;
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Smar clear from tmpBuffer - síma clear helyett
     * @param tmpBuffer
     */
    public static void smartClear (ArrayList<Byte> tmpBuffer){
        while (tmpBuffer.size() > 0 && tmpBuffer.get(0).intValue() != 0x0A){
            tmpBuffer.remove(0);
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Delete x bytes from tmpBuffer
     * @param fullSize
     */
    public static void deleteFromTmp(ArrayList<Byte> tmpBuffer, int fullSize){
        //Utils.log("DELETE!");
        for (int i = 0; i < fullSize; i++) {
            if (tmpBuffer.size() > 0) {
                tmpBuffer.remove(0);
            }
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Merging incoming data to temp arraylist
     * @param serialIncomingData
     */
    public static void mergeToTemp(ArrayList<Byte> tmpBuffer, byte[] serialIncomingData){
        // Merge data to tempBuffer
        for (byte anSerialIncomingData : serialIncomingData) {
            tmpBuffer.add(anSerialIncomingData);
        }
    }

// -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Creating encoded tmp buffer to showing data in log
     * It is only for development
     * @return
     */
    public static byte[] createEncodedTmpBuffer(ArrayList<Byte> tmpBuffer){
        byte[] encodedTmpBuffer = new byte[tmpBuffer.size()];
        for (int i = 0; i < encodedTmpBuffer.length; i++) {
            encodedTmpBuffer[i] = tmpBuffer.get(i);
        }
        return encodedTmpBuffer;
    }

}
