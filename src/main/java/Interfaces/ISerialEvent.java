package Interfaces;

import com.fazecast.jSerialComm.SerialPortEvent;

public interface ISerialEvent {
    void onResponse(SerialPortEvent serialPortEvent);
}
